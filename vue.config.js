const { defineConfig } = require('@vue/cli-service')
const pxtorem = require('postcss-pxtorem')
module.exports = defineConfig({
  transpileDependencies: true,
  css: {
    sourceMap: true,
    loaderOptions: {
      sass: {
        sassOptions: {outputStyle: 'compressed'}
      },
      postcss: {
        postcssOptions: {
          plugins: [
            pxtorem({
              rootValue: '10',
              propList: ['*'],
              unitPrecision: 2,
              mediaQueries: false,
            }),
          ]
        }
      }
    }
  }
})
