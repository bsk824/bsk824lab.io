import { createWebHashHistory, createRouter } from "vue-router";

const routes = [
  {
    name: "GuideIndex",
    path: "/",
    component: () => import("@/guide/GuideIndex"),
    children: [
      { 
        name: "GuideInfo",
        path: "/", 
        components: { sub: () => import("@/guide/GuideInfo.vue") }
      },
      { 
        name: "GuideList",
        path: "/guide/list", 
        components: { sub: () => import("@/guide/GuideList.vue") }
      },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;